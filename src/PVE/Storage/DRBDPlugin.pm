package PVE::Storage::DRBDPlugin;

use strict;
use warnings;

use IO::File;

use PVE::Tools qw(run_command trim);
use PVE::Storage::Plugin;
use PVE::JSONSchema qw(get_standard_option);

use base qw(PVE::Storage::Plugin);

use Data::Dumper;
use PVE::DRBD::Utils;
use PVE::DRBD::Resource;
# Configuration

sub type {
    return 'drbd';
}

sub plugindata {
    return {
	content => [ {images => 1, rootdir => 1}, { images => 1 }],
    };
}

sub properties {
    return {
	res_name => {
	    description => "Link resource name.",
	    type => 'string',
	},
	base_volume => {
	    description => "Base volume. This volume is automatically activated.",
	    type => 'string', format => 'pve-volume-id',
	},
    };
}

sub options {
    return {
	res_name => { fixed => 1 },
	nodes => { optional => 1 },
	disable => { optional => 1 },
	content => { optional => 1 },
    };
}

# Storage implementation

sub on_add_hook { #Call when launching VM with drbd storage
    my ($class, $storeid, $scfg, %param) = @_;
    if (my $base = $scfg->{base_volume}) {
	my ($baseid, $volname) = PVE::Storage::parse_volume_id($base);

	my $cfg = PVE::Storage::config();
	my $basecfg = PVE::Storage::storage_config ($cfg, $baseid, 1);
	die "base storage ID '$baseid' does not exist\n" if !$basecfg;

	# we only support iscsi for now
	die "unsupported base type '$basecfg->{type}'"
	    if $basecfg->{type} ne 'iscsi';

	my $path = PVE::Storage::path($cfg, $base);

	PVE::Storage::activate_storage($cfg, $baseid);
    }

    return;
}

sub parse_volname {
    my ($class, $volname) = @_;

    my ($name, $vmid) = split("-", $volname);

    return ('images', $name, $vmid, undef, undef, undef, 'raw');
}

sub filesystem_path {
    my ($class, $scfg, $volname, $snapname) = @_;

    die "drbd snapshot is not implemented" if defined($snapname);

    my ($vtype, $name, $vmid) = $class->parse_volname($volname);

    my $res_name = $scfg->{res_name};
    my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);


    my $path = PVE::DRBD::Resource::getBase($cfg->{cfg}, "device");
    return wantarray ? ($path, $vmid, $vtype) : $path;
}

sub create_base {
    my ($class, $storeid, $scfg, $volname) = @_;

    die "can't create base images in drbd storage\n";
}

sub clone_image {
    my ($class, $scfg, $storeid, $volname, $vmid, $snap) = @_;

    die "can't clone images in drbd storage\n";
}

sub alloc_image { #Generate disk name
    my ($class, $storeid, $scfg, $vmid, $fmt, $name, $size) = @_;

    my $res_name = $scfg->{res_name};
    my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

    my $device = PVE::DRBD::Resource::getBase($cfg->{cfg}, "device");
    if ($device =~ /\/([^\/]+)$/) {
        $device = $1;
    }

    my $storages = PVE::DRBD::Utils::get_vm_storage_formated($vmid);
    my $id = (scalar (keys %{$storages}));

    $name = "$device-$vmid-disk-$id";
    return $name;
}

sub free_image {
    my ($class, $storeid, $scfg, $volname, $isBase) = @_;

    return undef;
}

my $check_tags = sub {
    my ($tags) = @_;

    return defined($tags) && $tags =~ /(^|,)pve-vm-\d+(,|$)/;
};

sub list_images { #List of disks in storage
    my ($class, $storeid, $scfg, $vmid, $vollist, $cache) = @_;

    my $res = [];
    my $vms = PVE::DRBD::Utils::get_vm_list();

    for my $id (keys %{$vms}){
        my $storages = PVE::DRBD::Utils::get_vm_storage_formated($id);

        for my $storage (keys %{$storages}){
            my $infos = $storages->{$storage};
            my $convert_size = PVE::DRBD::Utils::convert_to_bytes($infos->{size});
            push @$res, {
            volid => "$storage:".$infos->{name}, format => 'raw', size => $convert_size, vmid => $id,
            ctime => 0, state => "stopped",
            } if ($storage eq $storeid);
        }
    }

    return $res;
}

sub get_used_size {
     my $disk = shift;

    my $output = `df -B1 $disk 2>/dev/null | tail -1`;

    if ($? == 0) {
        my @fields = split /\s+/, $output;
        my $used = $fields[2];
        return $used;
    } else {
        return 0;
    }
}

sub get_available_size {
    my $disk = shift;

    my $formated = PVE::DRBD::Utils::format_disk($disk);
    my $lv_info = PVE::DRBD::Utils::get_lv_info($formated->{lv}, $formated->{vg});

    my $size = PVE::DRBD::Utils::convert_to_bytes($lv_info->{size});

    return $size;
}

sub status { #Status of storage
    my ($class, $storeid, $scfg, $cache) = @_;

    my $res_name = $scfg->{res_name};

    my $activ = 0;
    my $free = 0;
    my $used = 0;
    my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

    my $device = PVE::DRBD::Resource::getBase($cfg->{cfg}, "device");
    my $disk = PVE::DRBD::Resource::getBase($cfg->{cfg}, "disk");
    $free = get_available_size($disk);

    my $images = list_images(undef, $storeid);
    for my $disque (keys %{$images}) {
        $used += $disque->{size};
    }
    $activ = 1 if (-e $device);

	return ($free + $used, $free, $used, $activ);
}

sub activate_storage { #Called by on add hook, check if all is ok.
    my ($class, $storeid, $scfg, $cache) = @_;

    my $res_name = $scfg->{res_name};
    return print  "Can't find resource $res_name, is drbd storage $storeid exist ?\n\n" if (!$res_name);
    my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

    return print  "Can't find $res_name, do resource exist in conf.cfg file, path exist or resource is valid ?\n\n" if (! (exists $cfg->{cfg}));

    my $device = PVE::DRBD::Resource::getBase($cfg->{cfg}, "device");

    return print  "Can't find $device, is $res_name resource up? (drbdadm status)\n\n" if (!(-e $device));

    my $status = `drbdadm status $res_name 2>&1`;
    return print "Error while executing drbdadm status $res_name: $status\n" if ($? != 0);
    return print "Can't open $device, node isn\'t primary. (drbdadm status)\n\n" if (!($status =~ /\bPrimary\b/));
}

sub deactivate_storage {
    my ($class, $storeid, $scfg, $cache) = @_;

    #Nothing to do.
}

sub activate_volume {
    my ($class, $storeid, $scfg, $volname, $snapname, $cache) = @_;

    #Nothing to do.
}

sub deactivate_volume {
    my ($class, $storeid, $scfg, $volname, $snapname, $cache) = @_;

    #Nothing to do.
}

sub volume_resize {
    my ($class, $scfg, $storeid, $volname, $size, $running) = @_;

    die "Can't resize drbd volume: not implemented (see pve-storage/src/PVE/Storage/DRBDPlugin.pm - volume_resize fonction)";
}
sub volume_size_info {
    my ($class, $scfg, $storeid, $volname, $timeout) = @_;

    my $path = $class->filesystem_path($scfg, $volname);

    my $size = `blockdev --getsize64 $path`;
    chomp($size);

    return wantarray ? ($size, 'raw', 0, undef) : $size;
}

sub volume_snapshot {
    my ($class, $scfg, $storeid, $volname, $snap) = @_;

    die "drbd snapshot is not implemented";
}

sub volume_snapshot_rollback {
    my ($class, $scfg, $storeid, $volname, $snap) = @_;

    die "drbd snapshot rollback is not implemented";
}

sub volume_snapshot_delete {
    my ($class, $scfg, $storeid, $volname, $snap) = @_;

    die "drbd snapshot delete is not implemented";
}

sub volume_has_feature {
    my ($class, $scfg, $feature, $storeid, $volname, $snapname, $running) = @_;

    my $features = {
	copy => { base => 1, current => 1},
	rename => {current => 1},
    };

    my ($vtype, $name, $vmid, $basename, $basevmid, $isBase) =
	$class->parse_volname($volname);

    my $key = undef;
    if($snapname){
	$key = 'snap';
    }else{
	$key =  $isBase ? 'base' : 'current';
    }
    return 1 if $features->{$feature}->{$key};

    return undef;
}

sub volume_export_formats {
    my ($class, $scfg, $storeid, $volname, $snapshot, $base_snapshot, $with_snapshots) = @_;
    return () if defined($snapshot); # lvm-thin only
    return volume_import_formats($class, $scfg, $storeid, $volname, $snapshot, $base_snapshot, $with_snapshots);
}

sub volume_export {
    my ($class, $scfg, $storeid, $fh, $volname, $format, $snapshot, $base_snapshot, $with_snapshots) = @_;

    die "Can't export drbd volume: not implemented (see pve-storage/src/PVE/Storage/DRBDPlugin.pm - volume_export fonction)";
}

sub volume_import_formats {
    my ($class, $scfg, $storeid, $volname, $snapshot, $base_snapshot, $with_snapshots) = @_;
    return () if $with_snapshots; # not supported
    return () if defined($base_snapshot); # not supported
    return ('raw+size');
}

sub volume_import {
    my ($class, $scfg, $storeid, $fh, $volname, $format, $snapshot, $base_snapshot, $with_snapshots, $allow_rename) = @_;

    die "Can't import drbd volume: not implemented (see pve-storage/src/PVE/Storage/DRBDPlugin.pm - volume_import fonction)";
}

sub volume_import_write {
    my ($class, $input_fh, $output_file) = @_;

    die "Can't import write drbd volume: not implemented (see pve-storage/src/PVE/Storage/DRBDPlugin.pm - volume_import_write fonction)";
}

sub rename_volume {
    my ($class, $scfg, $storeid, $source_volname, $target_vmid, $target_volname) = @_;

    die "Can't rename drbd volume: not implemented (see pve-storage/src/PVE/Storage/DRBDPlugin.pm - rename_volume fonction)";
}

1;
