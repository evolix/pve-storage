# Documentation des Modifications Proxmox pour l'Intégration de DRBD

## 1. Modifications du fork

## pve-storage:

**Fonctionnement :**
- `Storage.pm` : Fichier central pour la gestion des types de stockage.
- `Storage/` : Contient les modules relatifs aux différents types de stockage.

**Modifications :**
- `Storage/DRBDPlugin.pm` : Nouveau fichier ajoutant le support du stockage DRBD.
- `Storage.pm` : Ajout du type de stockage DRBD dans la gestion centrale des types de stockage.

## 2. Instructions d'installation

Ce paquet est essentiel pour le fonctionnement de [pve-drbd](https://gitea.evolix.org/evolix/pve-drbd.git). Il est donc inutile de l'installer seul.

1. Télécharger les sources du paquet ([pve-storage](https://gitea.evolix.org/evolix/pve-storage.git))
2. Compiler et installer avec `make dinstall` ou `gbp buildpackage` (dans la branch debian)
